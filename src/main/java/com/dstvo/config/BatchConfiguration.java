package com.dstvo.config;

import com.dstvo.model.VasReceiveModel;
import com.dstvo.service.JobCompletionNotificationListener;
import com.dstvo.service.VasReceiveModelItemProcessor;
import com.dstvo.writer.RestTemplateItemWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.DefaultBatchConfigurer;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.json.JacksonJsonObjectReader;
import org.springframework.batch.item.json.JsonItemReader;
import org.springframework.batch.item.json.JsonObjectReader;
import org.springframework.batch.item.json.builder.JsonItemReaderBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.web.client.RestTemplate;

import javax.sql.DataSource;

@Slf4j
@Configuration
@EnableBatchProcessing
public class BatchConfiguration extends DefaultBatchConfigurer {

    @Value("${connect.subscription.url}")
    private String connectSubscriptionUrl;

    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;

    public BatchConfiguration(JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
    }

    @Override
    public void setDataSource(DataSource dataSource) {
        log.info("No Data source is available");
    }

    @Bean
    public JsonItemReader<VasReceiveModel> jsonItemReader() {
        Resource resource = new ClassPathResource("vas-data.json");
        JsonObjectReader<VasReceiveModel> jsonObjectReader = new JacksonJsonObjectReader<>(VasReceiveModel.class);
        return new JsonItemReaderBuilder<VasReceiveModel>().resource(resource).jsonObjectReader(jsonObjectReader).name("vasData").build();
    }

    @Bean
    public VasReceiveModelItemProcessor processor() {
        return new VasReceiveModelItemProcessor();
    }


    @Bean
    public RestTemplateItemWriter templateItemWriter(RestTemplate restTemplate) {

        return new RestTemplateItemWriter().url(connectSubscriptionUrl).getRestTemplate(restTemplate).build();
    }

    @Bean
    public Job importUserJob(JobCompletionNotificationListener listener, Step step1) {
        return jobBuilderFactory.get("importUserJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(step1)
                .end()
                .build();
    }

    @Bean
    public Step step1(RestTemplateItemWriter writer) {
        return stepBuilderFactory.get("step1")
                .<VasReceiveModel, VasReceiveModel>chunk(10)
                .reader(jsonItemReader())
                .processor(processor())
                .writer(writer)
                .build();
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplateBuilder().build();
    }
}
