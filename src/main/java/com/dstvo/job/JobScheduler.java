package com.dstvo.job;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@Configuration
@EnableScheduling
public class JobScheduler {


    private JobLauncher jobLauncher;
    private Job job;

    private AtomicBoolean enabled = new AtomicBoolean(false);

    private AtomicInteger batchRunCounter = new AtomicInteger(0);

    public JobScheduler(JobLauncher jobLauncher, Job job) {
        this.jobLauncher = jobLauncher;
        this.job = job;
    }


    @Scheduled(fixedRate = 2000)
    public void processEBSData() throws Exception {

        if (enabled.get()) {
            Date date = new Date();
            JobParameters param = new JobParametersBuilder().addDate("launchDate", date).toJobParameters();
            jobLauncher.run(job, param);
            batchRunCounter.incrementAndGet();
            log.info("Batch run count: {}", batchRunCounter.get());
        }
    }

    public boolean getStatus() {
        return enabled.get();
    }

    public void enableOrDisable(boolean isEnable) {
        enabled.set(isEnable);
        log.info("Running Status: {}", isEnable);
    }

    public AtomicInteger getBatchRunCounter() {
        return batchRunCounter;
    }
}
