package com.dstvo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class EBSDataBatchProcessingApplication {

    public static void main(String[] args) {
        SpringApplication.run(EBSDataBatchProcessingApplication.class, args);
    }
}