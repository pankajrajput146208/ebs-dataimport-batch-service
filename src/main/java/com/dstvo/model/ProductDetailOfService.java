package com.dstvo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NegativeOrZero;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class ProductDetailOfService {

    @JsonProperty("Name")
    private String name;

    @JsonProperty("Description")
    private String description;

    @NotNull
    @JsonProperty("VASCategory")
    private String vasCategory;
}
