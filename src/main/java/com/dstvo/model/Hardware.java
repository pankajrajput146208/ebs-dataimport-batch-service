package com.dstvo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;

@Data
@NoArgsConstructor
public class Hardware {

    @JsonProperty("SerialNumber")
    private String serialNumber;

    @JsonProperty("Status")
    private String status;

    @JsonProperty("LinkedSmartcard")
    private String linkedSmartCard;

    @JsonProperty("ProductDetail")
    private ProductDetailOfHardware productDetailOfHardware;

    @Valid
    @JsonProperty("ListOfService")
    private ServiceCategories serviceCategories;

}