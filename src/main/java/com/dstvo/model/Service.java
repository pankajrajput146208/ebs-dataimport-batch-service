package com.dstvo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class Service {

    @JsonProperty("UID")
    private String uid;

    @NotNull
    @JsonProperty("Status")
    private String status;

    @NotNull
    @Valid
    @JsonProperty("ProductDetail")
    private ProductDetailOfService productDetailOfService;

    @NotNull
    @JsonProperty("AgreementId")
    private String agreementId;

}