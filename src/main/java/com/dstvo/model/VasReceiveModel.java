package com.dstvo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;


@Data
@NoArgsConstructor
public class VasReceiveModel {

    @NotNull
    @JsonProperty("CustomerUID")
    private String customerUID;

    @NotNull
    @JsonProperty("Country")
    private int country;

    @NotNull
    @Valid
    @JsonProperty("ListOfHardware")
    private HardwareCategories hardwareCategories;

    @NotNull
    private Date customerUpdateTimestamp;

}