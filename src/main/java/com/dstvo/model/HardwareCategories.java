package com.dstvo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
public class HardwareCategories {

    @NotNull
    @Valid
    @JsonProperty("Hardware")
    List<Hardware> hardwareList;
}
