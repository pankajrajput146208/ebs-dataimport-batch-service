package com.dstvo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Collection;

// To run this service locally, create a database named "ddm-product-codes" and a collection call "productCode"

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductModel {

    private String productCode;

    @NotNull
    @NotEmpty
    private Collection<String> packages;

}
