package com.dstvo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
public class ServiceCategories {

    @NotNull
    @Valid
    @JsonProperty("Service")
    private List<Service> serviceList;
}
