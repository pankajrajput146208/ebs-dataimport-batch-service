package com.dstvo.api;

import com.dstvo.job.JobScheduler;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SchedulerApi {

    private final JobScheduler jobScheduler;

    public SchedulerApi(JobScheduler jobScheduler) {
        this.jobScheduler = jobScheduler;
    }

    @GetMapping(value = "/enable/{runningStatus}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Boolean> enableOrDisable(@PathVariable String runningStatus) {
        jobScheduler.enableOrDisable(Boolean.parseBoolean(runningStatus));
        return ResponseEntity.ok(true);
    }

    @GetMapping(value = "/get-status", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Boolean> enableOrDisable() {
        return ResponseEntity.ok(jobScheduler.getStatus());
    }
}
