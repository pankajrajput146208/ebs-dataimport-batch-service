package com.dstvo.service;

import com.dstvo.model.VasReceiveModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;

@Slf4j
public class VasReceiveModelItemProcessor implements ItemProcessor<VasReceiveModel, VasReceiveModel> {

    @Override
    public VasReceiveModel process(final VasReceiveModel vasReceiveModel) {
        /* Adding "-" with customer id to identify the ebs migration customer in subscription event publisher  */
        log.info("Adding migration identifier: -");
        final String customerId = vasReceiveModel.getCustomerUID().concat("-");
        vasReceiveModel.setCustomerUID(customerId);
        return vasReceiveModel;
    }

}
