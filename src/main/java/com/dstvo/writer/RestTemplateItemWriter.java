package com.dstvo.writer;

import com.dstvo.model.VasReceiveModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;
import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Slf4j
public class RestTemplateItemWriter implements ItemWriter<VasReceiveModel> {
    private String url;
    private RestTemplate restTemplate;

    @Override
    public void write(List<? extends VasReceiveModel> items) {
        items.forEach(this::sendToSubscriptionEventService);
    }

    public RestTemplateItemWriter getRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        return this;
    }

    private RestTemplateItemWriter sendToSubscriptionEventService(VasReceiveModel vasReceiveModel) {
        HttpEntity<VasReceiveModel> request = new HttpEntity<>(vasReceiveModel);
        restTemplate.postForObject(url, request, VasReceiveModel.class);
        log.info("Completed: {}", vasReceiveModel);
        return this;
    }

    public RestTemplateItemWriter url(String url) {
        this.url = url;
        return this;
    }

    public RestTemplateItemWriter build() {
        return this;
    }
}
